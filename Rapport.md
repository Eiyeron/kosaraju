Rapport du TP Kosaraju & 2SAT
===============

Florian Dormont et Victor Grousset
---------------

Le projet a été fini et les tests fournis dans les annexes du sujet obtiennent les résultats prévus (sauf pour les graphes les plus gros où un StackOverflow sauvage apparaît mais c'est "normal"). Le projet utilise les librairies [JUnit] et [AssertJ] ainsi que [Maven] uniquement pour faciliter le développment en utilisant des tests unitaires et d'intégration, Maven pour avoir une structure de programme et pour obtenir automatiquement les dépendances des librairies utilisées. Ils n'intéragissent en aucun cas avec les mécanismes du projet.

Lien vers le dépôt git du code source : [Gitlab](https://gitlab.com/Eiyeron/kosaraju/).

Utilisation du programme : 
```bash
    java -jar kosaraju.jar <filename>
```

[JUnit]: (http://junit.org/)
[AssertJ]: (https://joel-costigliola.github.io/assertj/)
[Maven]: (https://maven.apache.org/)
