package org.syntax_error.kosaraju;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.FileNotFoundException;
import java.io.InputStream;

import org.junit.Test;

public class Sat2GraphBuilderFromStreamTest {

	@Test
	public void testSolvable6Edges() throws FileNotFoundException {
		Sat2Graph graph = buildSat2GraphFromFileName("/sat_2_examples/sat6");
		assertThat(graph.isSolvable()).isTrue();
	}

	@Test
	public void testSolvable10Edges() throws FileNotFoundException {
		Sat2Graph graph = buildSat2GraphFromFileName("/sat_2_examples/sat10");
		assertThat(graph.isSolvable()).isTrue();
	}

	@Test
	public void testSolvable100Edges() throws FileNotFoundException {
		Sat2Graph graph = buildSat2GraphFromFileName("/sat_2_examples/sat100");
		assertThat(graph.isSolvable()).isTrue();
	}

	@Test
	public void testSolvable1000Edges() throws FileNotFoundException {
		Sat2Graph graph = buildSat2GraphFromFileName("/sat_2_examples/sat1000");
		assertThat(graph.isSolvable()).isTrue();
	}

	@Test
	public void testUnsolvable6Edges() throws FileNotFoundException {
		Sat2Graph graph = buildSat2GraphFromFileName("/sat_2_examples/unsat6");
		assertThat(graph.isSolvable()).isFalse();
	}

	@Test
	public void testUnsolvable20Edges() throws FileNotFoundException {
		Sat2Graph graph = buildSat2GraphFromFileName("/sat_2_examples/unsat20");
		assertThat(graph.isSolvable()).isFalse();
	}

	@Test
	public void testUnsolvable1000Edges() throws FileNotFoundException {
		Sat2Graph graph = buildSat2GraphFromFileName("/sat_2_examples/unsat1000");
		assertThat(graph.isSolvable()).isFalse();
	}

	private Sat2Graph buildSat2GraphFromFileName(String fileName) {
		InputStream inputSteam = this.getClass().getResourceAsStream(fileName);
		Sat2GraphBuilderFromStream graphBuilder = new Sat2GraphBuilderFromStream(inputSteam);
		Sat2Graph graph = graphBuilder.parse();
		return graph;
	}
}
