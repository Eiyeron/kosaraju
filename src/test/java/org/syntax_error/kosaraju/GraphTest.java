package org.syntax_error.kosaraju;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GraphTest {
	private Graph graph;

	@Before
	public void setUp() throws Exception {
		graph = new Graph();
	}

	@Test
	public void testCreation() {
		assertEquals("[]", graph.toString());
	}

	@Test
	public void testAdd() {
		graph.addVertex(0);
		assertEquals("[0:{}]", graph.toString());
	}

	@Test
	public void testAddNegativeNumber() {
		graph.addVertex(-1);
		assertEquals("[-1:{}]", graph.toString());
	}

	@Test
	public void testAddUpTo() {
		graph.addVerticesUpTo(1);
		assertEquals("[0:{}, 1:{}]", graph.toString());
	}

	@Test
	public void testCreateArrowFromAtoB() throws Exception {
		graph.addVerticesUpTo(1);
		graph.createArrowFromAtoB(0, 1);
		assertEquals("[0:{1}, 1:{}]", graph.toString());
	}

	@Test
	public void testCreateMultiplesArrows() throws Exception {
		graph.addVerticesUpTo(2);
		graph.createArrowFromAtoB(0, 2);
		graph.createArrowFromAtoB(0, 1);
		graph.createArrowFromAtoB(1, 0);
		graph.createArrowFromAtoB(1, 2);
		assertEquals("[0:{2, 1}, 1:{0, 2}, 2:{}]",
				graph.toString());
	}

	@Test
	public void testTransposed() throws Exception {
		graph.addVerticesUpTo(2);
		graph.createArrowFromAtoB(0, 2);
		graph.createArrowFromAtoB(0, 1);
		graph.createArrowFromAtoB(1, 0);
		graph.createArrowFromAtoB(1, 2);
		Graph transposed = graph.getTransposed();
		assertEquals("[0:{1}, 1:{0}, 2:{0, 1}]",
				transposed.toString());
	}

	@Test
	public void testAddVerticeTwiceShouldNotResetTheNeighbors() throws Exception {
		graph.addVertex(1);
		graph.addVertex(2);
		graph.createArrowFromAtoB(1, 2);
		graph.addVertex(1);
		assertEquals("[1:{2}, 2:{}]", graph.toString());
	}
	@Test
	public void testGetNeighbors() throws Exception {
		graph.addVertex(2);
		graph.addVertex(3);
		graph.createArrowFromAtoB(2, 3);
		assertEquals("[3]", graph.getNeighbors(2).toString());
	}
}
