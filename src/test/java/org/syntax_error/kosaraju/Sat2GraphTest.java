package org.syntax_error.kosaraju;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Sat2GraphTest {
	private Sat2Graph sat2Graph;

	@Before
	public void setUp() throws Exception {
		sat2Graph = new Sat2Graph();
	}

	@Test
	public void testGraphConsistency() throws Exception {
		sat2Graph.addSatArrows(1, -2);
		sat2Graph.addSatArrows(1, 3);
		sat2Graph.addSatArrows(-1, -3);

		String expectedGraph = "[-1:{-2, 3}, 1:{-3}, -2:{}, 2:{1}, -3:{1}, 3:{-1}]";
		assertEquals(expectedGraph, sat2Graph.getGraph().toString());
	}

	@Test
	public void testIsSolvableShouldBeTrue() {
		sat2Graph.addSatArrows(1, -2);
		sat2Graph.addSatArrows(1, 3);
		sat2Graph.addSatArrows(-1, -3);
		assertThat(sat2Graph.isSolvable()).isTrue();
	}

	@Test
	public void testIsSolvableBeTrue2() {
		sat2Graph.addSatArrows(-4, -1);
		sat2Graph.addSatArrows(2, 5);
		sat2Graph.addSatArrows(1, 5);
		sat2Graph.addSatArrows(-6, -4);
		sat2Graph.addSatArrows(2, 4);
		sat2Graph.addSatArrows(-1, 5);
		sat2Graph.addSatArrows(-5, -3);
		sat2Graph.addSatArrows(-3, -2);
		sat2Graph.addSatArrows(-6, -5);

		sat2Graph.addSatArrows(4, 6);
		sat2Graph.addSatArrows(-6, 4);
		sat2Graph.addSatArrows(-1, 4);

		assertThat(sat2Graph.isSolvable()).isTrue();
	}

}
