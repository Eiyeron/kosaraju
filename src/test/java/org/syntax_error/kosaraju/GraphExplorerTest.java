package org.syntax_error.kosaraju;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public class GraphExplorerTest {
	private Graph graph;
	private GraphExplorer explorer;

	@Before
	public void setUp() {
		graph = new Graph();
		graph.addVerticesUpTo(4);
		graph.createArrowFromAtoB(0, 1);
		graph.createArrowFromAtoB(0, 2);
		graph.createArrowFromAtoB(2, 1);
		graph.createArrowFromAtoB(2, 3);
		explorer = new GraphExplorer(graph);
	}

	@Test
	public void testGetReachableVerticesFrom() {
		assertEquals("[0, 1, 2, 3]", explorer.getReachableVerticesFrom(0).toString());
		assertEquals("[1]", explorer.getReachableVerticesFrom(1).toString());
		assertEquals("[2, 1, 3]", explorer.getReachableVerticesFrom(2).toString());
		assertEquals("[3]", explorer.getReachableVerticesFrom(3).toString());
	}

	@Test
	public void testDepthFirstSearchWholeGraph() {
		graph.addVerticesUpTo(4);
		graph.createArrowFromAtoB(3, 4);
		graph.createArrowFromAtoB(4, 2);
		ArrayList<Integer> expectedOrderedVertices = new ArrayList<>(Arrays.asList(1, 4, 3, 2, 0));
		ArrayList<Integer> result = explorer.iteratedDepthFirstSearch();
		assertEquals(expectedOrderedVertices, result);
	}

	@Test
	public void testDepthFirstSearchWholeGraphWithFirstVertexIsolated() {
		graph = buildGraphWithFirstVertexIsolated();
		explorer = new GraphExplorer(graph);

		ArrayList<Integer> expectedOrderedVertices = new ArrayList<>(Arrays.asList(0, 2, 5, 4, 3, 1));
		ArrayList<Integer> result = explorer.iteratedDepthFirstSearch();
		assertEquals(expectedOrderedVertices, result);
	}

	@Test
	public void testStronglyConnectedComponentsSimple() {
		graph = buildGraphWithFirstVertexIsolated();
		explorer = new GraphExplorer(graph);

		ArrayList<ArrayList<Integer>> stronglyConnectedComponents = explorer.getStronglyConnectedComponents();

		String expectedFirstStronglyConnectedComponents = "[[1], [4, 5, 3], [2], [0]]";
		assertEquals(expectedFirstStronglyConnectedComponents, stronglyConnectedComponents.toString());
	}

	@Test
	public void testStronglyConnectedComponentsComplexGraph() {
		graph = buildGraphFromSpecExample();
		explorer = new GraphExplorer(graph);
		ArrayList<ArrayList<Integer>> stronglyConnectedComponents = explorer.getStronglyConnectedComponents();

		String expectedFirstStronglyConnectedComponents = "[[16, 15, 14, 13], [12], [9, 11, 10, 8, 7, 6], [5, 4, 3, 2], [1], [0]]";
		assertEquals(expectedFirstStronglyConnectedComponents, stronglyConnectedComponents.toString());
	}

	@Test
	public void testStronglyConnectedComponentsComplexGraph2() {
		graph = buildGraphFromSpecExample2();
		explorer = new GraphExplorer(graph);
		ArrayList<ArrayList<Integer>> stronglyConnectedComponents = explorer.getStronglyConnectedComponents();

		String expectedFirstStronglyConnectedComponents = "[[8], [2], [7, 6, 4, 3], [5], [9, 11, 10, 1]]";
		assertEquals(expectedFirstStronglyConnectedComponents, stronglyConnectedComponents.toString());
	}

	@Test
	public void testStronglyConnectedComponentsCircularGraph() throws Exception {
		graph = new Graph();
		graph.addVerticesUpTo(2);

		graph.createArrowFromAtoB(0, 1);
		graph.createArrowFromAtoB(1, 2);
		graph.createArrowFromAtoB(2, 0);

		explorer = new GraphExplorer(graph);
		ArrayList<ArrayList<Integer>> stronglyConnectedComponents = explorer.getStronglyConnectedComponents();

		String expectedFirstStronglyConnectedComponent = "[[1, 2, 0]]";
		assertEquals(expectedFirstStronglyConnectedComponent, stronglyConnectedComponents.toString());
	}

	@Test
	public void testStronglyConnectedComponentsCircularGraph2() throws Exception {
		graph = new Graph();
		for (int i = 1; i < 4; i++) {
			graph.addVertex(i);
		}

		graph.createArrowFromAtoB(1, 2);
		graph.createArrowFromAtoB(2, 3);
		graph.createArrowFromAtoB(3, 1);

		explorer = new GraphExplorer(graph);
		ArrayList<ArrayList<Integer>> stronglyConnectedComponents = explorer.getStronglyConnectedComponents();

		String expectedFirstStronglyConnectedComponent = "[[2, 3, 1]]";
		assertEquals(expectedFirstStronglyConnectedComponent, stronglyConnectedComponents.toString());
	}

	private Graph buildGraphFromSpecExample2() {
		graph = new Graph();
		for (int i = 1; i < 12; i++) {
			graph.addVertex(i);
		}

		graph.createArrowFromAtoB(1, 9);
		graph.createArrowFromAtoB(1, 11);

		graph.createArrowFromAtoB(2, 3);
		graph.createArrowFromAtoB(2, 7);

		graph.createArrowFromAtoB(3, 1);
		graph.createArrowFromAtoB(3, 6);

		graph.createArrowFromAtoB(4, 3);
		graph.createArrowFromAtoB(4, 7);
		graph.createArrowFromAtoB(4, 10);
		graph.createArrowFromAtoB(4, 11);

		graph.createArrowFromAtoB(6, 4);
		graph.createArrowFromAtoB(6, 5);
		graph.createArrowFromAtoB(6, 11);

		graph.createArrowFromAtoB(7, 3);
		graph.createArrowFromAtoB(7, 5);
		graph.createArrowFromAtoB(7, 6);

		graph.createArrowFromAtoB(8, 10);
		graph.createArrowFromAtoB(8, 11);

		graph.createArrowFromAtoB(9, 10);

		graph.createArrowFromAtoB(10, 1);

		graph.createArrowFromAtoB(11, 10);

		return graph;
	}

	private Graph buildGraphFromSpecExample() {
		graph = new Graph();
		graph.addVerticesUpTo(16);

		graph.createArrowFromAtoB(2, 1);
		graph.createArrowFromAtoB(2, 4);

		graph.createArrowFromAtoB(3, 2);
		graph.createArrowFromAtoB(3, 5);

		graph.createArrowFromAtoB(4, 1);
		graph.createArrowFromAtoB(4, 3);

		graph.createArrowFromAtoB(5, 4);

		graph.createArrowFromAtoB(6, 8);

		graph.createArrowFromAtoB(7, 6);
		graph.createArrowFromAtoB(7, 10);

		graph.createArrowFromAtoB(8, 7);

		graph.createArrowFromAtoB(9, 1);
		graph.createArrowFromAtoB(9, 7);
		graph.createArrowFromAtoB(9, 11);

		graph.createArrowFromAtoB(10, 8);
		graph.createArrowFromAtoB(10, 9);

		graph.createArrowFromAtoB(11, 10);

		graph.createArrowFromAtoB(12, 1);
		graph.createArrowFromAtoB(12, 11);

		graph.createArrowFromAtoB(13, 16);

		graph.createArrowFromAtoB(14, 6);
		graph.createArrowFromAtoB(14, 13);

		graph.createArrowFromAtoB(15, 2);
		graph.createArrowFromAtoB(15, 7);
		graph.createArrowFromAtoB(15, 14);

		graph.createArrowFromAtoB(16, 3);
		graph.createArrowFromAtoB(16, 15);

		return graph;
	}

	private Graph buildGraphWithFirstVertexIsolated() {
		graph = new Graph();
		graph.addVerticesUpTo(5);

		graph.createArrowFromAtoB(1, 2);
		graph.createArrowFromAtoB(1, 3);
		graph.createArrowFromAtoB(3, 2);
		graph.createArrowFromAtoB(3, 4);
		graph.createArrowFromAtoB(4, 5);
		graph.createArrowFromAtoB(5, 3);

		return graph;
	}
}
