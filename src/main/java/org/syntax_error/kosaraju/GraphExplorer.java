package org.syntax_error.kosaraju;

import java.util.ArrayList;
import java.util.Collections;

public class GraphExplorer {
	private final Graph graph;

	public GraphExplorer(Graph graph) {
		this.graph = graph;
	}

	public ArrayList<Integer> iteratedDepthFirstSearch() {
		ArrayList<Integer> orderedVertices = new ArrayList<>();
		ArrayList<Integer> inProgressVertices = new ArrayList<>();

		for (int vertexNumber : graph.getVertices().keySet()) {
			depthFirstSearch(vertexNumber, orderedVertices, inProgressVertices);
			if(!orderedVertices.contains(vertexNumber)) {
				orderedVertices.add(vertexNumber);
			}
		}
		return orderedVertices;
	}

	public ArrayList<ArrayList<Integer>> getStronglyConnectedComponents() {
		ArrayList<Integer> orderedVertices = iteratedDepthFirstSearch();
		ArrayList<Integer> reverseOrderedVertices = reverseArrayList(orderedVertices);

		Graph transposedGraph = graph.getTransposed();
		GraphExplorer transposedGraphExplorer = new GraphExplorer(transposedGraph);

		ArrayList<Integer> inProgressVertices = new ArrayList<>();
		ArrayList<ArrayList<Integer>> result = new ArrayList<>();
		for (int vertex : reverseOrderedVertices) {
			if (inProgressVertices.contains(vertex)) {
				continue;
			}
			ArrayList<Integer> stronglyConnectedComponent = new ArrayList<>();
			transposedGraphExplorer.depthFirstSearch(vertex, stronglyConnectedComponent, inProgressVertices);
			stronglyConnectedComponent.add(vertex);
			result.add(stronglyConnectedComponent);
		}
		return result;
	}

	private void depthFirstSearch(int vertex, ArrayList<Integer> orderedVertices,
			ArrayList<Integer> inProgressVertices) {
		if (inProgressVertices.contains(vertex)) {
			return;
		}
		inProgressVertices.add(vertex);
		if (graph.getNeighbors(vertex).isEmpty()) {
			return;
		}

		for (int neighbor : graph.getNeighbors(vertex)) {
			if (inProgressVertices.contains(neighbor) || orderedVertices.contains(neighbor)) {
				continue;
			}
			depthFirstSearch(neighbor, orderedVertices, inProgressVertices);
			orderedVertices.add(neighbor);
		}
	}

	private ArrayList<Integer> reverseArrayList(ArrayList<Integer> orderedVertices) {
		ArrayList<Integer> verticesForSecondIteratedDepthFirstSearch = new ArrayList<Integer>(orderedVertices);
		Collections.reverse(verticesForSecondIteratedDepthFirstSearch);
		return verticesForSecondIteratedDepthFirstSearch;
	}

	// Method only for tests purposes
	ArrayList<Integer> getReachableVerticesFrom(int id) {
		ArrayList<Integer> orderedVertices = new ArrayList<>();
		ArrayList<Integer> visitedVertices = new ArrayList<Integer>();
		depthFirstSearch(id, orderedVertices, visitedVertices);

		return visitedVertices;
	}
}
