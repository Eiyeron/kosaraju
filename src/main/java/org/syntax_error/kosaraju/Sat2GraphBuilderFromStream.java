package org.syntax_error.kosaraju;

import java.io.InputStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sat2GraphBuilderFromStream {
	/**
	 * This regex matches real decimal numbers in a possibly garbaged/incorrect
	 * String when a Scanner chokes and throws exceptions.
	 *
	 * <pre>
	 * (-?(?:\.|\d+\.?)\d*)
	 * (
	 *  -?       <=> Zero or one negative sign
	 *  The next (part) matches the integer part of a number with a dot if it exists or just the point.
	 *  (?:      <=> Unsaved match.
	 *    \.     <=> Just a dot
	 *    |      <=> or
	 *    \d+\.? <=> One or more number with a zero or one dot.
	 * )         <=> End of the integer matching part
	 * \d*       <=> Zero or more numbers. Matches the decimal part of the number.
	 * </pre>
	 */
	static Pattern numberCollector = Pattern
			.compile("(-?(?:\\.\\d*|\\d+\\.?)\\d*)");

	private InputStream input;

	private Sat2Graph sat2Graph;

	public Sat2GraphBuilderFromStream(InputStream input) {
		this.input = input;
		this.sat2Graph = new Sat2Graph();
	}

	public Sat2Graph parse() {
		Scanner scan = new Scanner(input);

		while (scan.hasNext()) {
			String line = scan.nextLine();
			Matcher matcher = numberCollector.matcher(line);
			if (matcher.find()) {
				int origin = Integer.valueOf(matcher.group());
				int target;
				if (matcher.find()) {
					target = Integer.valueOf(matcher.group());

					sat2Graph.addSatArrows(origin, target);
				}
			}
		}
		scan.close();
		return sat2Graph;
	}

}
