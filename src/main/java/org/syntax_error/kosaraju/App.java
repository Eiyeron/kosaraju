package org.syntax_error.kosaraju;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class App {
	public static void main(String[] args) {
		checkArgs(args);
		String filePath = args[0];

		Sat2Graph graph = buildGraph(filePath);
		System.out.println("Succesfully parsed file: \n" + filePath);
		evaluateSovability(graph);
	}

	private static void checkArgs(String[] args) {
		if(args.length != 1) {
			System.err.println("Usage: kosaraju.jar /path/of/file/with/2SAT/formula");
			System.exit(1);
		}
	}

	private static Sat2Graph buildGraph(String filePath) {
		Sat2Graph graph = null;
		try {
			graph = buildSat2GraphFromFileName(filePath);
		} catch (FileNotFoundException e) {
			System.err.println("File not found, try again, path entered was:");
			System.err.println(filePath);
			System.exit(1);
		}
		return graph;
	}

	private static void evaluateSovability(Sat2Graph graph) {
		if(graph.isSolvable()) {
			System.out.println("And it's solvable");
		} else {
			System.out.println("And it's not solvable");
		}
	}

	private static Sat2Graph buildSat2GraphFromFileName(String filePath)
			throws FileNotFoundException {
		File file = new File(filePath);
		InputStream inputSteam = new FileInputStream(file);
		Sat2GraphBuilderFromStream graphBuilder = new Sat2GraphBuilderFromStream(inputSteam);
		Sat2Graph graph = graphBuilder.parse();
		return graph;
	}
}
