package org.syntax_error.kosaraju;

import java.util.HashMap;
import java.util.LinkedList;

public class Graph {
	private HashMap<Integer, LinkedList<Integer>> vertices;

	public Graph(int size) {
		vertices = new HashMap<Integer, LinkedList<Integer>>(size);
	}

	public Graph() {
		this(0);
	}

	public void addVertex(int key) {
		if(!vertices.containsKey(key)) {
			vertices.put(key, new LinkedList<Integer>());
		}
	}

	/**
	 * Add vertices up to max with keys from 0 to max-1
	 * @param max
	 */
	public void addVerticesUpTo(int max) {
		int newSize = max + 1;
		if (vertices.size() >= newSize)
			return;
		for (int i = vertices.size(); i < newSize; i++) {
			addVertex(i);
		}
	}

	public void createArrowFromAtoB(int a, int b) {
		if (vertices.get(a).contains(b))
			return;
		vertices.get(a).add(b);
	}

	public LinkedList<Integer> getNeighbors(int id) {
		return vertices.get(id);
	}

	public HashMap<Integer, LinkedList<Integer>> getVertices() {
		return vertices;
	}


	public Graph getTransposed() {
		Graph transposed = new Graph();

		for (int vertexToCreate : vertices.keySet()) {
			transposed.addVertex(vertexToCreate);
		}

		for (int a : vertices.keySet()) {
			for (int b : vertices.get(a)) {
				transposed.createArrowFromAtoB(b, a);
			}
		}
		return transposed;
	}

	@Override
	public String toString() {
		String result = "[";
		boolean shouldAddCommaBetweenVertices = false;
		for (int i : vertices.keySet()) {
			if (shouldAddCommaBetweenVertices) {
				result += ", ";
			}
			shouldAddCommaBetweenVertices = true;
			result += i + ":{";
			boolean shouldAddCommaForArrows = false;
			for (int index : vertices.get(i)) {
				if (shouldAddCommaForArrows) {
					result += ", ";
				}
				shouldAddCommaForArrows = true;
				result += index;
			}
			result += "}";
		}
		result += "]";
		return result;
	}
}
