package org.syntax_error.kosaraju;

import java.util.ArrayList;

public class Sat2Graph {

	private Graph graph;

	public Sat2Graph(Graph graph) {
		this.graph = graph;
	}

	public Sat2Graph() {
		this(new Graph());
	}

	public Graph getGraph() {
		return graph;
	}

	public void addSatArrows(int a, int b) {
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(-a);
		graph.addVertex(-b);
		graph.createArrowFromAtoB(-a, b);
		graph.createArrowFromAtoB(-b, a);
	}

	public boolean isSolvable() {
		// TODO get strongly connected components on graph
		// interate in each component
		// if found value and -value
		// NOT SOLVABLE

		ArrayList<ArrayList<Integer>> connexGroup = new GraphExplorer(graph)
				.getStronglyConnectedComponents();
		for (ArrayList<Integer> component : connexGroup) {
			ArrayList<Integer> presentValues = new ArrayList<Integer>();
			for (int value : component) {
				if (presentValues.contains(Math.abs(value))) {
					return false;
				}
				presentValues.add(Math.abs(value));
			}
		}
		return true;
	}
}
